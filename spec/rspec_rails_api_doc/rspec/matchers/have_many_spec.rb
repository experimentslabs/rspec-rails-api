# frozen_string_literal: true

require 'spec_helper'

require 'json'

require 'rspec/rails/api/metadata'
require 'rspec/rails/api/matchers'
require 'rspec/rails/api/entity_config'

FakeResponse = Struct.new :body

RSpec.describe '"have_many" matcher' do # rubocop:disable RSpec/DescribeClass
  context 'with simple objects' do
    # Use string keys, as in hashes from JSON responses
    let(:valid_entity) { { 'id' => 10, 'name' => 'John Doe' } }

    let(:entity_description) do
      RSpec::Rails::Api::EntityConfig.new(
        id:   { type: :integer, description: 'Identifier' },
        name: { type: :string, description: 'The name' }
      ).to_h
    end

    it 'matches valid entity' do
      expect([valid_entity]).to have_many entity_description
    end

    it 'handles "response"' do
      response = FakeResponse.new [valid_entity].to_json
      expect(response).to have_many entity_description
    end

    it 'rejects entities with a wrong type' do # rubocop:disable RSpec/MultipleExpectations
      expect do
        # Expecting with the tested matcher
        expect([valid_entity, { 'id' => 10, 'name' => 100 }]).to have_many entity_description
      end.to raise_error RSpec::Expectations::ExpectationNotMetError
    end

    it 'rejects entities with missing attributes' do # rubocop:disable RSpec/MultipleExpectations
      expect do
        # Expecting with the tested matcher
        expect([valid_entity, { 'id' => 10 }]).to have_many entity_description
      end.to raise_error RSpec::Expectations::ExpectationNotMetError
    end
  end

  context 'with deep objects' do
    let(:entity_description) do
      RSpec::Rails::Api::EntityConfig.new(
        name:    { type: :string, description: 'Name' },
        friends: {
          type: :array, description: 'The friends', of: {
            name: { type: :string,  description: 'The name' },
          }
        },
        cat:     {
          type: :object,  description: 'The cat', attributes: {
            name: { type: :string,  description: 'Its name' },
            age:  { type: :integer, description: 'Its age' },
          }
        }
      ).to_h
    end

    it 'validates deep objects' do # rubocop:disable RSpec/ExampleLength
      response = [
        {
          'name'    => 'John',
          'friends' => [
            { 'name' => 'Jessie' },
            { 'name' => 'Frank' },
          ],
          'cat'     => {
            'name' => 'Mr Bigglesworth',
            'age'  => 10,
          },
        },
      ]

      expect(response).to have_many entity_description
    end

    it 'fails with errors in deep objects' do # rubocop:disable RSpec/ExampleLength, RSpec/MultipleExpectations
      response = [
        {
          'name'    => 'John',
          'friends' => [
            { 'name' => 'Jessie' },
            { 'name' => 'Frank' },
          ],
          'cat'     => {
            'name' => 'Mr Bigglesworth',
            'age'  => 'Hello',
          },
        },
      ]

      expect do
        # Expecting with the tested matcher
        expect(response).to have_many entity_description
      end.to raise_error RSpec::Expectations::ExpectationNotMetError
    end

    it 'fails with errors in deep arrays' do # rubocop:disable RSpec/ExampleLength, RSpec/MultipleExpectations
      response = [
        {
          'name'    => 'John',
          'friends' => [
            { 'name' => 'Jessie', 'age' => 10 },
            { 'name' => 'Frank' },
          ],
          'cat'     => {
            'name' => 'Mr Bigglesworth',
            'age'  => 10,
          },
        },
      ]
      expect do
        # Expecting with the tested matcher
        expect(response).to have_many entity_description
      end.to raise_error RSpec::Expectations::ExpectationNotMetError
    end
  end
end
