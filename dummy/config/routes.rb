# frozen_string_literal: true

Rails.application.routes.draw do
  resources :posts

  get "/static/mixed_list", to: "static#mixed_list"
  get "/static/string", to: "static#string"
  get "/static/string_list", to: "static#string_list"
  get "/static/custom_error", to: "static#custom_error"
  get "/static/complex", to: "static#complex"
  post "/static/with_file", to: "static#with_file"
  get "/static/query_params/:id", to: "static#query_params"
  get "/static/custom_content_type", to: "static#custom_content_type"
  get "/static/any_types", to: "static#any_types"
end
