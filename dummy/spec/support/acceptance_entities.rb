# frozen_string_literal: true

# Global entities. You may want to split those into multiple files in your projects.

# Constant used here so we can include the file in the library specs without side effects
ACCEPTANCE_ENTITIES = {
  error: {
    error: { type: :string, description: "Error message" }
  },
  form_error: {
    title: { type: :array, required: false, description: "Title errors", of: :string }
  },
  custom_error: {
    error: { type: :string, description: "Error message" },
    context: { type: :string, description: "Additional context" }
  },
  thing: {
    id: { type: :integer, description: "Identifier" },
    name: { type: :string, description: "Thing name" }
  },
  dummy: {
    id: { type: :integer, description: "Identifier" },
    description: { type: :string, description: "Dummy description" },
    things: { type: :array, of: :thing, description: "A list of things" }
  },
  stuff: {
    error_obj: { type: :object, attributes: :error, description: "An error of a kind" },
    another_error: { type: :object, attributes: :custom_error, description: "An custom error" }
  },
  complex_thing: {
    strings: { type: :array, description: "List of strings", of: :string },
    objects: { type: :array, description: "List of objects by reference", of: :thing },
    friends: { type: :array, description: "List of objects with attributes", of: {
      first_name: { type: :string, description: "First name" },
      last_name: { type: :string, required: false, description: "Last name" },
    } },
    thing: { type: :object, attributes: :thing, description: "A Thing" },
    dummy: { type: :object, attributes: :dummy, description: "A Dummy" },
    sub_dummy: { type: :object, description: "Something that contains a referenced object", attributes: {
      the_dummy: { type: :object, attributes: :dummy, description: "The sub-dummy" },
    } },
    errors: { type: :object, attributes: :stuff, description: "My nice error collection" }
  },
  data_with_file_name: {
    title: { type: :string, description: "The title" },
    some_nested: { type: :object, description: "Nested file upload test", attributes: {
      file_name: { type: :string, description: "Uploaded file name" }
    } }
  },
  query_and_path_response: {
    id: { type: :string, description: "Identifier" },
    name: { type: :string, description: "Name" },
    other_param: { type: :string, required: false, description: "A flag" },
  },
  any_types: {
    anything: { type: :any, description: "Any type but null" },
    anything_optional: { type: :any, required: false, description: "Any type but null" },
    list: { type: :array, description: "An array of anything but nulls" },
    nested: { type: :object, description: "An object", attributes: {
      anything: { type: :any, description: "Anything but nulls" },
    } },
  }
}
