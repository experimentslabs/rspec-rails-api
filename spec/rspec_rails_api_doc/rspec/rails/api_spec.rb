# frozen_string_literal: true

RSpec.describe RSpec::Rails::Api do
  it 'has a version number' do
    expect(RSpec::Rails::Api::VERSION).not_to be_nil
  end
end
