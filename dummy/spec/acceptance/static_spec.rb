# frozen_string_literal: true

require "rails_helper"

RSpec.describe StaticController do
  # Needed to initialize documentation for this type of request.
  # It should come before all the other calls (it will let the metadata
  # class know which resource we're dealing with)
  resource "Static", <<~MD
    Collection of endpoints that returns static data, only for testing.
  MD

  on_get "/static/string_list", "A list of strings" do
    for_code 200, expect_many: :string do |url|
      test_response_of url
    end
  end

  on_get "/static/string", "A string" do
    for_code 200, expect_one: :string do |url|
      test_response_of url
    end
  end

  on_get "/static/mixed_list", "Mixed data types list" do
    for_code 200, "check post content", expect_one: :array do |url|
      test_response_of url
    end
  end

  on_get "/static/custom_error", "An error defined by a local entity" do
    for_code 500, expect_one: :custom_error do |url|
      test_response_of url
    end
  end

  on_get "/static/complex", "An error defined by a local entity" do
    for_code 200, expect_one: :complex_thing do |url|
      test_response_of url
    end
  end

  on_post "/static/with_file", "File upload", <<~MD do
    When using the `file` type for a request parameter, it changes the request's content type to `multipart/form-data`.
  MD
    request_params attributes: {
      title: { type: :string, description: "The title" },
      some_nested: { type: :object, description: "A sub-object for testing", attributes: {
        file: { type: :file, description: "File to upload" }
      } }
    }

    for_code 201, expect_one: :data_with_file_name do |url|
      test_response_of url, payload: { title: "My picture", some_nested: { file:  Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/text_file.txt"), "text/plain") } }
    end
  end

  on_get "/static/query_params/:id", "Query and path parameters" do
    path_params fields: {
      # Path interpolation
      id: { type: :integer, description: "Unique identifier" },
      # Query string
      'filters[name]': { type: :string, description: "Filter property" },
      other_param: { type: :boolean, required: false, description: "A simple flag" }
    }

    for_code 200, expect_one: :query_and_path_response do |url|
      test_response_of url, path_params: { "filters[name]" => "Bob", id: 10 }
    end
  end

  on_get "/static/custom_content_type", "A response with a custom content type" do
    for_code 200, expect_one: :string, with_content_type: "application/json+custom; charset=utf-8" do |url|
      test_response_of url
    end
  end

  on_get "/static/any_types", "A response to test the 'any' type" do
    for_code 200, expect_one: :any_types do |url|
      test_response_of url
    end
  end
end
