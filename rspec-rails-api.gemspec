# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rspec/rails/api/version'

Gem::Specification.new do |spec|
  spec.name    = 'rspec-rails-api'
  spec.version = RSpec::Rails::Api::VERSION
  spec.authors = ['Manuel Tancoigne']
  spec.email   = ['m.tancoigne@gmail.com']

  spec.summary     = 'Tests standard Rails API responses and generate doc'
  spec.description = <<~TXT
    Create acceptance tests to check the Rails API responses and generate
    documentation from it.
  TXT
  spec.homepage = 'https://gitlab.com/experimentslabs/rspec-rails-api'
  spec.license  = 'MIT'
  spec.metadata = {
    'source_code_uri'       => 'https://gitlab.com/experimentslabs/rspec-rails-api',
    'bug_tracker_uri'       => 'https://gitlab.com/experimentslabs/rspec-rails-api/issues',
    'changelog_uri'         => 'https://gitlab.com/experimentslabs/rspec-rails-api/blob/master/CHANGELOG.md',
    'rubygems_mfa_required' => 'true',
  }

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features|dummy)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 3.1.2'
end
