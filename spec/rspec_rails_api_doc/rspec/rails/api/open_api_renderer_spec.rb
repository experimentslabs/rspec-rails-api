# frozen_string_literal: true

require 'yaml'
require 'json-schema'
require 'spec_helper'

require 'rspec/rails/api/open_api_renderer'
require 'rspec/rails/api/metadata'
require_relative '../../../../../dummy/spec/support/acceptance_entities'

RSpec.describe RSpec::Rails::Api::OpenApiRenderer do
  after do
    RSpec::Rails::Api::Metadata.reset
  end

  describe '.merge_context' do
    let(:renderer) do
      instance = described_class.new
      # Configure it like in the dummy application
      instance.add_security_scheme :basic, { type: 'http', scheme: 'basic' }
      instance.add_security_scheme :api_key, name: 'API key authentication', type: 'apiKey', in: 'header'
      instance.redact_responses custom_error: { context: 'FILTERED' }

      # Manually declare all entities as they are now stored in Metadata object
      ACCEPTANCE_ENTITIES.merge({ post: {
                                  id:         { type: :integer, description: 'Post identifier' },
                                  title:      { type: :string, description: 'Post title' },
                                  content:    { type: :string, description: 'Post content' },
                                  created_at: { type: :datetime, description: 'Creation date' },
                                  updated_at: { type: :datetime, description: 'Last change date' },
                                  url:        { type: :string, description: 'URL to this post' },
                                } }).each do |name, attributes|
        RSpec::Rails::Api::Metadata.add_entity name, attributes
      end

      instance
    end

    context 'with valid context metadata' do
      let(:meta) do
        renderer.merge_context YAML.load_file File.join('dummy', 'tmp', 'rra_metadata.yaml')
        renderer.prepare_metadata
      end

      it 'sets the data correctly' do
        expected = YAML.load_file File.join('dummy', 'public', 'swagger_doc.yaml')
        expect(meta).to eq expected
      end

      it 'defaults POST/PUT responses to application/json' do
        aggregate_failures do
          expect(meta.dig('paths', '/posts', 'post', 'requestBody', 'content')&.keys).to eq ['application/json']
          expect(meta.dig('paths', '/posts/{id}', 'put', 'requestBody', 'content')&.keys).to eq ['application/json']
        end
      end

      context 'with a file' do
        it 'changes the request content-type' do
          keys = meta.dig('paths', '/static/with_file', 'post', 'requestBody', 'content')&.keys
          expect(keys).to eq ['multipart/form-data']
        end

        it 'specifies the format' do
          schema_properties = meta.dig 'components', 'schemas', 'post__static_with_file', 'properties', 'some_nested', 'properties', 'file'

          aggregate_failures do
            expect(schema_properties['format']).to eq 'binary'
            expect(schema_properties['type']).to eq 'string'
          end
        end
      end
    end
  end

  describe 'generated dummy OpenAPI documentation' do
    it 'validates against the schema' do
      hash = YAML.load_file File.join('dummy', 'public', 'swagger_doc.yaml')
      schema = JSON.parse(File.read(File.join('spec/fixtures/openapi-schema.json')))

      expect(JSON::Validator.validate(schema, hash)).to be true
    end
  end
end
