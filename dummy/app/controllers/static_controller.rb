# frozen_string_literal: true

class StaticController < ApplicationController
  def string_list
    render json: ["a", "b", "c"]
  end

  def string
    render json: '"Hello"'
  end

  def mixed_list
    render json: ["a", 1, true]
  end

  def custom_error
    render json: { error: "Something bad happened", context: "... but was made on purpose" }, status: :internal_server_error
  end

  def complex
    render json: {
      strings: ["a", "b", "c"],
      thing: { id: 1, name: "A Thing" },
      dummy: { id: 1, description: "Something weird", things: [{ id: 2, name: "The second Thing" }] },
      sub_dummy: { the_dummy: { id: 1, description: "Something weird", things: [{ id: 2, name: "The second Thing" }] } },
      objects: [
        { id: 1, name: "A thing" },
        { id: 2, name: "Another thing" },
      ],
      friends: [
        { first_name: "Bob", last_name: "Doe" },
        { first_name: "Alice", last_name: "Doe" },
      ],
      errors: {
        error_obj: { error: "Something bad happened" },
        another_error: { error: "a custom error", context: "with context" }
      },
    }
  end

  def with_file
    data = { title: params[:title], some_nested: { file_name: params[:some_nested][:file][:original_filename]  } }
    render json: data, status: :created
  end

  def query_params
    render json: {
      id: params[:id],
      name: params[:filters][:name],
      other_param: params[:other_param],
    }
  end

  def custom_content_type
    render json: '"Great!"', content_type: "application/json+custom"
  end

  def any_types
    render json: {
      anything: "string",
      anything_optional: nil,
      list: [
        "string",
        nil,
        1,
        true,
        false,
        ["string"],
        { key: "value" },
      ],
      nested: { anything: false }
    }
  end
end
