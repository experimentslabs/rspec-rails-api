# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rails/api/utils'

RSpec.describe RSpec::Rails::Api::Utils do
  let(:hash) { { a: 1, b: 2, c: { ca: 1, 'cb' => 2 } } }

  describe '#deep_set' do
    it 'sets values if parent does not exist' do
      new_hash = described_class.deep_set hash, %w[some sub_key], 'value'
      expect(new_hash).to eq(a: 1, b: 2, c: { ca: 1, 'cb' => 2 }, some: { sub_key: 'value' })
    end

    it 'overwrites parents if parent are no hash' do
      new_hash = described_class.deep_set hash, %w[a sub_key], 'value'
      expect(new_hash).to eq(a: { sub_key: 'value' }, b: 2, c: { ca: 1, 'cb' => 2 })
    end

    it "don't overwrite parent if it's a hash" do
      new_hash = described_class.deep_set hash, %w[c sub_key], 'value'
      expect(new_hash).to eq(a: 1, b: 2, c: { ca: 1, 'cb' => 2, sub_key: 'value' })
    end

    it 'creates symbol keys' do
      new_hash = described_class.deep_set hash, %w[c cb], 'value'
      expect(new_hash).to eq(a: 1, b: 2, c: { ca: 1, 'cb' => 2, cb: 'value' })
    end
  end
end
