# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rails/api/metadata'

RSpec.describe RSpec::Rails::Api::Metadata do
  let(:meta) { described_class.new }

  before do
    described_class.reset
  end

  describe '.add_resource' do
    it 'adds an empty resource' do
      name        = 'Users'
      description = 'User management'
      meta.add_resource name, description
      expect(meta.resources).to eq(Users: { description: description, paths: {} })
    end

    it 'sets the current resource' do
      name        = 'Users'
      description = 'User management'
      meta.add_resource name, description
      expect(meta.current_resource).to eq(name.to_sym)
    end

    context 'when the resource exists' do
      it 'does not change anything' do
        name        = 'Users'
        description = 'User management'
        meta.add_resource name, description
        meta.add_resource name, 'New description'
        expect(meta.resources[:Users][:description]).to eq description
      end
    end
  end

  describe '#add_entity' do
    context 'with simple valid params' do
      it 'adds the entity in current resource' do
        described_class.add_entity(:entity, id: { type: :integer, required: false, description: 'something' })

        expect(described_class.entities[:entity]).to be_an RSpec::Rails::Api::EntityConfig
      end
    end

    context 'with invalid type' do
      it 'raises an error' do
        expect do
          described_class.add_entity(:entity, id: { type: :something_invalid, required: false, description: 'something' })
        end.to raise_error("Field type not allowed: 'something_invalid'")
      end
    end
  end

  describe '.add_parameter' do
    it 'makes every field required by default' do
      meta.add_parameter :test, id: { type: :integer }
      expect(meta.parameters[:test][:id][:required]).to be true
    end

    it 'does not change optional fields' do
      meta.add_parameter :test, id: { type: :integer, required: false }
      expect(meta.parameters[:test][:id][:required]).to be false
    end
  end

  describe '.add_path_params' do
    let(:fields) { { id: { type: :string, description: 'The user to display' } } }

    context 'with a resource declared' do
      let(:meta) do
        meta = described_class.new
        meta.add_resource 'Users', 'User management'

        meta
      end

      context 'with an action declared' do
        let(:meta) do
          meta = described_class.new
          meta.add_resource 'Users', 'User management'
          meta.add_action :get, '/users/:id', 'Display an user'

          meta
        end

        context 'with valid parameters' do # rubocop:disable RSpec/NestedGroups
          it 'adds a path parameter' do
            meta.add_path_params fields
            resources = meta.resources
            value     = resources[:Users][:paths][:'/users/:id'][:path_params]
            expect(value).to eq(id: { description: 'The user to display', type: :string, required: true, scope: :path })
          end
        end

        context 'with invalid options' do # rubocop:disable RSpec/NestedGroups
          let(:fields) { { id: { type: :object, description: 'The user to display' } } }

          it 'raises an error' do
            expect do
              meta.add_path_params fields
            end.to raise_error 'Field type not allowed: object'
          end
        end

        context 'with parameters not in path' do # rubocop:disable RSpec/NestedGroups
          let(:fields) { { not_in_url: { type: :string, description: 'Querystring parameter' } } }

          it 'adds it to the query parameters' do
            meta.add_path_params fields

            resources = meta.resources
            value     = resources[:Users][:paths][:'/users/:id'][:path_params]
            expect(value).to eq(not_in_url: { description: 'Querystring parameter', type: :string, required: true, scope: :query })
          end
        end
      end

      context 'without an action declared' do
        it 'raises an error' do
          expect do
            meta.add_path_params fields
          end.to raise_error 'No url declared'
        end
      end
    end

    context 'without a resource declared' do
      it 'raises an error' do
        expect do
          meta.add_path_params fields
        end.to raise_error 'No resource declared'
      end
    end
  end

  describe '.add_request_params' do
    let(:fields) do
      {
        user: {
          type: :object, attributes: {
            name: { type: :string, description: 'The new name' },
          }
        },
      }
    end

    context 'with a resource declared' do
      context 'with an action declared' do
        let(:meta) do
          meta = described_class.new
          meta.add_resource 'Users', 'User management'
          meta.add_action :post, '/users/:id', 'Creates an user'

          meta
        end

        context 'with valid parameters' do # rubocop:disable RSpec/NestedGroups
          it 'adds a request parameter' do
            meta.add_request_params fields
            resources = meta.instance_variable_get(:@resources)
            value     = resources[:Users][:paths][:'/users/:id'][:actions][:post][:params]
            expect(value).to eq(properties: { user: { properties: { name: { description: 'The new name', type: 'string' } }, required: ['name'] } }, required: ['user'])
          end
        end

        context 'with invalid options' do # rubocop:disable RSpec/NestedGroups
          let(:fields) { { id: { type: :something_invalid, description: 'The user to display' } } }

          it 'raises an error' do
            expect do
              meta.add_request_params fields
            end.to raise_error 'Field type not allowed: something_invalid'
          end
        end
      end

      context 'without an action declared' do
        let(:meta) do
          meta = described_class.new
          meta.add_resource 'Users', 'User management'

          meta
        end

        it 'raises an error' do
          expect do
            meta.add_request_params fields
          end.to raise_error 'No action declared'
        end
      end
    end

    context 'without a resource declared' do
      it 'raises an error' do
        expect do
          meta.add_request_params fields
        end.to raise_error 'No resource declared'
      end
    end
  end

  describe '.add_security_reference' do
    let(:meta) do
      meta = described_class.new
      meta.add_resource 'Users', 'User management'
      meta.add_action :post, '/users/:id', 'Creates an user'

      meta
    end

    it 'adds the security scheme reference' do
      meta.add_security_references :some_defined_scheme, :another_security_scheme
      resources = meta.instance_variable_get(:@resources)
      value     = resources[:Users][:paths][:'/users/:id'][:actions][:post][:security]
      expect(value).to eq(%i[some_defined_scheme another_security_scheme])
    end
  end

  describe '.add_action' do
    let(:method) { :get }
    let(:url) { '/api/users' }
    let(:summary) { 'List users' }

    context 'with a resource declared' do
      let(:meta) do
        meta = described_class.new
        meta.add_resource 'Users', 'User management'

        meta
      end

      it 'adds an action' do # rubocop:disable RSpec/ExampleLength
        meta.add_action method, url, summary
        resources       = meta.resources
        expected_config = {
          url.to_sym => {
            actions: {
              method.to_sym => {
                description: '', summary: summary, statuses: {}, params: {}
              },
            },
          },
        }
        expect(resources[:Users][:paths]).to eq(expected_config)
      end

      it 'sets the current url and method' do # rubocop:disable RSpec/MultipleExpectations
        meta.add_action method, url, summary
        expect(meta.current_url).to eq url
        expect(meta.current_method).to eq method
      end
    end

    context 'without a resource declared' do
      it 'raises an error' do
        expect do
          meta.add_action method, url, summary
        end.to raise_error 'No resource declared'
      end
    end
  end

  describe '.add_status_code' do
    let(:code) { 200 }
    let(:description) { 'Success response' }

    context 'with a resource declared' do
      let(:meta) do
        meta = described_class.new
        meta.add_resource 'Users', 'User management'

        meta
      end

      context 'with an action declared' do
        let(:meta) do
          meta = described_class.new
          meta.add_resource 'Users', 'User management'
          meta.add_action :get, '/users', 'List users'

          meta
        end

        it 'adds a status code' do # rubocop:disable RSpec/ExampleLength
          meta.add_status_code code, description

          resources       = meta.resources
          expected_config = {
            '/users': {
              actions: {
                get: {
                  description: '',
                  summary:     'List users',
                  params:      {},
                  statuses:    {
                    '200': {
                      description: 'Success response',
                      example:     { response: nil },
                    },
                  },
                },
              },
            },
          }
          expect(resources[:Users][:paths]).to eq(expected_config)
        end

        it 'sets the current status code' do
          meta.add_status_code code, description
          expect(meta.current_code).to eq code
        end
      end

      context 'without an action declared' do
        it 'raises an error' do
          expect do
            meta.add_status_code code, description
          end.to raise_error 'No action declared'
        end
      end
    end

    context 'without a resource declared' do
      it 'raises an error' do
        expect do
          meta.add_status_code code, description
        end.to raise_error 'No resource declared'
      end
    end
  end

  describe '.add_request_example' do
    let(:request_example) do
      { url: '/users', action: :get, status_code: 200, response: '', path_params: nil, params: nil }
    end

    context 'with a resource declared' do
      let(:meta) do
        meta = described_class.new
        meta.add_resource 'Users', 'User management'

        meta
      end

      context 'with an action declared' do
        let(:meta) do
          meta = described_class.new
          meta.add_resource 'Users', 'User management'
          meta.add_action :get, '/users', 'Display an user'

          meta
        end

        context 'with a code declared' do # rubocop:disable RSpec/NestedGroups
          let(:meta) do
            meta = described_class.new
            meta.add_resource 'Users', 'User management'
            meta.add_action :get, '/users', 'Display users list'
            meta.add_status_code 200, 'Success response'

            meta
          end

          context 'with valid parameters' do # rubocop:disable RSpec/NestedGroups
            it 'add a request example' do
              meta.add_request_example(**request_example)
              resources = meta.resources
              value     = resources[:Users][:paths][:'/users'][:actions][:get][:statuses][:'200'][:example]

              expect(value).to eq(params: nil, path_params: nil, response: '')
            end
          end
        end

        context 'without a code declared' do # rubocop:disable RSpec/NestedGroups
          it 'raises an error' do
            expect do
              meta.add_request_example(**request_example)
            end.to raise_error 'Resource not found for GET /users'
          end
        end
      end

      context 'without an action declared' do
        it 'raises an error' do
          expect do
            meta.add_request_example(**request_example)
          end.to raise_error 'Resource not found for GET /users'
        end
      end
    end

    context 'without a resource declared' do
      it 'raises an error' do
        expect do
          meta.add_request_example(**request_example)
        end.to raise_error 'Resource not found for GET /users'
      end
    end
  end

  describe '.to_h' do
    it 'returns a hash' do
      expect(meta.to_h).to be_a Hash
    end

    it 'has the right keys' do
      expect(meta.to_h.keys).to eq(%i[resources entities])
    end
  end

  describe '.organize_params' do
    it 'creates a hash with required fields' do # rubocop:disable RSpec/ExampleLength
      result = meta.send :organize_params,
                         id:   { type: :integer },
                         name: { type: :string, required: false }

      expected = {
        properties: {
          id:   { type: 'integer', description: nil },
          name: { type: 'string', description: nil },
        },
        required:   ['id'],
      }
      expect(result).to eq expected
    end

    it 'handles arrays values with full definition' do # rubocop:disable RSpec/ExampleLength
      result = meta.send :organize_params,
                         comments: { type: :array, of: { id: { type: :integer } } }
      expected = {
        properties: { comments: {
          description: nil,
          items:       {
            properties: { id: { description: nil, type: 'integer' } },
            required:   ['id'],
          },
          type:        'array',
        } },
        required:   ['comments'],
      }

      expect(result).to eq expected
    end

    it 'handles "simple" values' do # rubocop:disable RSpec/ExampleLength
      result = meta.send :organize_params, comments: { type: :array, of: :string }
      expected = {
        properties: { comments: {
          description: nil,
          items:       :string,
          type:        'array',
        } },
        required:   ['comments'],
      }

      expect(result).to eq expected
    end
  end
end
