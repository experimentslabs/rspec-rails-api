# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rails/api/entity_config'

RSpec.describe RSpec::Rails::Api::EntityConfig do
  describe '.initialize' do
    it 'adds fields' do # rubocop:disable RSpec/ExampleLength, RSpec/MultipleExpectations
      fields = {
        id:   { type: :number,  description: 'Identifier' },
        name: { type: :string,  description: 'The name' },
      }

      entity_config = described_class.new(fields)

      keys = entity_config.fields.keys
      expect(keys.count).to eq 2
      expect(keys.sort).to eq %i[id name]
      expect(entity_config.fields[:id]).to be_an RSpec::Rails::Api::FieldConfig
    end
  end

  describe '.to_h' do
    it 'creates a valid hash' do # rubocop:disable RSpec/ExampleLength
      entity = described_class.new(
        id:      { type: :number, required: false, description: 'Identifier' },
        name:    { type: :string, required: false, description: 'The name' },
        friends: { type: :array, required: false, description: 'Friends list' },
        dog:     { type: :object, required: false, description: 'The dog', attributes: :dog_entity },
        cat:     {
          type: :object, required: false, description: 'The cat', attributes: {
            name: { type: :string, required: false, description: 'Cat name' },
          }
        }
      )

      expected_hash = {
        id:      { type: :number, required: false, description: 'Identifier' },
        name:    { type: :string, required: false, description: 'The name' },
        friends: { type: :array, required: false, description: 'Friends list' },
        dog:     { type: :object, required: false, description: 'The dog', attributes: :dog_entity },
        cat:     {
          type: :object, required: false, description: 'The cat', attributes: {
            name: { type: :string, required: false, description: 'Cat name' },
          }
        },
      }

      expect(entity.to_h).to eq expected_hash
    end
  end

  describe '.with' do
    it 'completes the entity' do # rubocop:disable RSpec/ExampleLength
      entities = {
        friend: described_class.new(
          name: { type: :string, description: 'Friend name' }
        ),
        dog:    described_class.new(
          name:  { type: :string, description: 'Doggo name' },
          color: { type: :string, required: false, description: 'Doggo color' }
        ),
      }

      entity = described_class.new(
        id:      { type: :number, required: false, description: 'Identifier' },
        name:    { type: :string, required: false, description: 'The name' },
        friends: { type: :array, required: false, description: 'Friends list', of: :friend },
        pseudos: { type: :array, required: false, description: 'Nicknames', of: :string },
        dog:     { type: :object, required: false, description: 'The dog', attributes: :dog }
      )

      expected_hash = {
        id:      { type: :number, required: false, description: 'Identifier' },
        name:    { type: :string, required: false, description: 'The name' },
        friends: {
          type: :array, required: false, description: 'Friends list', attributes: {
            name: { type: :string, required: true, description: 'Friend name' },
          }
        },
        pseudos: {
          type: :array, required: false, description: 'Nicknames', attributes: { type: :string }
        },
        dog:     {
          type: :object, required: false, description: 'The dog', attributes: {
            name:  { type: :string, required: true, description: 'Doggo name' },
            color: { type: :string, required: false, description: 'Doggo color' },
          }
        },
      }

      expect(entity.expand_with(entities)).to eq expected_hash
    end
  end
end
