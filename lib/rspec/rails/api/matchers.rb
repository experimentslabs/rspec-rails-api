# frozen_string_literal: true

require 'active_support/hash_with_indifferent_access'
require 'yaml'

require 'rspec/rails/api/validator'
require 'rspec/rails/api/utils'

##
# RSpec matcher to check something against an array of `expected`
RSpec::Matchers.define :have_many do |expected|
  match do |actual|
    actual = RSpec::Rails::Api::Utils.hash_from_response actual

    raise "Response is not an array: #{actual.class}" unless actual.is_a? Array
    raise 'Response has no item to compare with' unless actual.count.positive?

    @errors = RSpec::Rails::Api::Validator.validate_array actual, expected

    @errors.blank?
  end

  failure_message do |actual|
    object = RSpec::Rails::Api::Utils.hash_from_response(actual).to_json.chomp
    RSpec::Rails::Api::Validator.format_failure_message @errors, object
  end
end

##
# RSpec matcher to check something against the `expected` definition
RSpec::Matchers.define :have_one do |expected|
  match do |actual|
    actual = RSpec::Rails::Api::Utils.hash_from_response actual

    @errors = if expected.keys.count == 1 && expected.key?(:type)
                RSpec::Rails::Api::Validator.validate_type actual, expected[:type]
              else
                RSpec::Rails::Api::Validator.validate_object actual, expected
              end

    @errors.blank?
  end

  failure_message do |actual|
    object = RSpec::Rails::Api::Utils.hash_from_response(actual).to_json.chomp
    RSpec::Rails::Api::Validator.format_failure_message @errors, object
  end
end
