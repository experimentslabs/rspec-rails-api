# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :error_not_found

private
  def error_not_found(message)
    render json: { error: message }, status: :not_found
  end
end
