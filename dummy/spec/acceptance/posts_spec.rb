# frozen_string_literal: true

require "rails_helper"

RSpec.describe PostsController do
  # Needed to initialize documentation for this type of request.
  # It should come before all the other calls (it will let the metadata
  # class know which resource we're dealing with)
  resource "Posts", <<~MD
    All endpoints to manage the posts of this site.

    This is a long description that supports **Markdown**.
  MD

  # Define an entity that can be returned by an URL.
  entity :post,
          id: { type: :integer, description: "Post identifier" },
          title: { type: :string, description: "Post title" },
          content: { type: :string, description: "Post content" },
          created_at: { type: :datetime, description: "Creation date" },
          updated_at: { type: :datetime, description: "Last change date" },
          url: { type: :string, description: "URL to this post" }

  parameters :form_params,
             post: { type: :object, description: "The post", attributes: {
                 title:   { type: :string, description: "Post title" },
                 content: { type: :string, required: false, description: "Post content" }
             } }

  parameters :post_path_params,
             id: { type: :integer, description: "Post identifier" }

  # This variable matches one of the path params, so its value will be used
  # when running the examples. If you need a different value (i.e.: for failures),
  # you still can manually define them when calling `test_response_of`
  let(:id) { Post.first.id }

  # You may also define things like custom payloads or other useful variables here

  before do
    FactoryBot.create_list :post, 2
  end

  on_get "/posts", "Posts list" do
    # Status description is optional: If nothing is provided, a human readable
    # string will be used
    for_code 200, expect_many: :post do |url|
      # Actually visit the path. It will expect the given code and a
      # content of type "application/JSON" (except for 204: no content
      # statuses)
      test_response_of url

      # You can make other expectations, even if that's not the role of these tests
    end
  end

  # Declare a path accessed with a given method
  # Their values can be declared with a  "let(:var){ value }" statement,
  # or passed to "test_response_of" method (see "for_code 404")
  on_get "/posts/:id", "One post" do
    # Use of in-place path parameter. Use `parameter` method to DRY.
    path_params fields: { id: { type: :integer, description: "Post identifier" } }

    for_code 200, "Success", expect_one: :post do |url|
      test_response_of url
    end

    # The "test_only" option will run the test without adding it to the documentation
    for_code 200, "check post content", expect_one: :post, test_only: true do |url|
      test_response_of url
      # Useless test to demonstrate usage of "test_only"
      expect(JSON.parse(response.body)["id"]).to eq id
    end

    for_code 404, "Invalid id", expect_one: :error do |url|
      test_response_of url, path_params: { id: 0 }
    end
  end

  on_post "/posts", "Create post" do
    # Use of in-place attributes. Use `parameter` method to DRY.
    request_params attributes: {
        post: { type: :object, description: "The new post", attributes: {
            title:   { type: :string, description: "Post title" },
            content: { type: :string, required: false, description: "Post content" }
        } }
    }

    requires_security :basic, :api_key

    for_code 201, "Post created", expect_one: :post do |url|
      test_response_of url, payload: { post: { title: "A new post !", content: "It has content" } }
    end

    for_code 422, "Invalid form data", expect_one: :form_error do |url|
      test_response_of url, payload: { post: { title: "", content: "It has content" } }
    end
  end

  on_put "/posts/:id", "Update post", <<~MD do
    Updates a post with new data.

    **Markdown** may be allowed here, depending on the tool used to render the
    documentation.
  MD

    requires_security :basic

    # Use of predefined path parameters
    path_params defined: :post_path_params
    # Use of predefined request parameters
    request_params defined: :form_params

    for_code 200, "Post updated", expect_one: :post do |url|
      test_response_of url, payload: { post: { title: "A new title !", content: "It has new content" } }
    end

    for_code 404, "Invalid id", expect_one: :error do |url|
      test_response_of url, path_params: { id: 0 }, payload: { post: { title: "A new title !", content: "It has new content" } }
    end

    for_code 422, "Invalid form data", expect_one: :form_error do |url|
      test_response_of url, payload: { post: { title: "", content: "It has content" } }
    end
  end

  on_delete "/posts/:id", "Destroy post" do
    # Not checking content for 204 responses
    for_code 204, "Post destroyed" do |url|
      test_response_of url
    end

    for_code 404, "Invalid id", expect_one: :error do |url|
      test_response_of url, path_params: { id: 0 }
    end
  end
end
