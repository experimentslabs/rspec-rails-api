# frozen_string_literal: true

require 'active_support/hash_with_indifferent_access'

module RSpec
  module Rails
    module Api
      # Helper methods
      class Utils
        class << self
          ##
          # Sets a value at given dotted path in a hash
          #
          # @param hash      [Hash]   The target hash
          # @param path      [Array]  List of keys to access value
          # @param value     [*]      Value to set
          #
          # @return [Hash] The modified hash
          def deep_set(hash, path, value)
            raise 'path should be an array' unless path.is_a? Array

            return value if path.count.zero?

            current_key       = path.shift.to_s.to_sym
            hash[current_key] = {} unless hash[current_key].is_a?(Hash)
            hash[current_key] = deep_set(hash[current_key], path, value)

            hash
          end

          ##
          # Returns a hash from an object
          #
          # @param value [Hash,Class] A hash or something with a "body" (as responses object in tests)
          #
          # @return [Hash]
          def hash_from_response(value)
            return JSON.parse(value.body) if value.respond_to? :body

            value
          end
        end
      end
    end
  end
end
