# frozen_string_literal: true

require 'spec_helper'

require 'json'

require 'rspec/rails/api/metadata'
require 'rspec/rails/api/matchers'

FakeResponse = Struct.new :body

RSpec.describe '"have_one" matcher' do # rubocop:disable RSpec/DescribeClass
  context 'with simple objects' do
    # Use string keys, as in hashes from JSON responses
    let(:valid_entity) { { 'id' => 10, 'name' => 'John Doe' } }

    let(:entity_description) do
      RSpec::Rails::Api::EntityConfig.new(
        id:   { type: :integer, description: 'Identifier' },
        name: { type: :string, description: 'Identifier' }
      ).to_h
    end

    it 'matches valid entity' do
      expect(valid_entity).to have_one entity_description
    end

    it 'handles "response"' do
      response = FakeResponse.new valid_entity.to_json
      expect(response).to have_one entity_description
    end

    it 'fails when entity is invalid' do # rubocop:disable RSpec/ExampleLength, RSpec/MultipleExpectations
      expected_error = <<~TXT
        expected object structure not to have these errors:
          name: is missing

        As a notice, here is the JSON object:
          {"id":10}
      TXT

      expect do
        valid_entity = { 'id' => 10 }
        # Expecting with the tested matcher
        expect(valid_entity).to have_one entity_description
      end.to raise_error expected_error
    end
  end
end
