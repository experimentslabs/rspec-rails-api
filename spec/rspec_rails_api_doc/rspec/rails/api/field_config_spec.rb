# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rails/api/field_config'

RSpec.describe RSpec::Rails::Api::FieldConfig do
  it 'handle simple fields' do # rubocop:disable RSpec/MultipleExpectations
    field_config = described_class.new(type: :number, required: false, description: 'Identifier')

    expect(field_config.description).to eq('Identifier')
    expect(field_config.type).to eq(:number)
  end

  it 'handles array fields' do
    field_config = described_class.new(type: :array, required: false, description: 'Friends list')
    expect(field_config.type).to eq(:array)
  end

  it 'handles object fields' do # rubocop:disable RSpec/ExampleLength
    field_config = described_class.new(
      type: :object, description: 'The dog', required: false, attributes: {
        name: { type: :string, description: 'Dog name', required: false },
      }
    )

    expect(field_config.attributes).to be_an RSpec::Rails::Api::EntityConfig
  end

  it 'handles object fields with symbol' do
    field_config = described_class.new(type: :object, description: 'The cat', attributes: :cat_entity)

    expect(field_config.attributes).to be_a Symbol
  end

  context 'when definition is invalid' do
    context 'with an object and "of"' do
      it 'raises an error' do
        expect do
          described_class.new(type: :object, description: 'The bat', of: :bat_entity)
        end.to raise_error "Don't use 'of' on non-arrays"
      end
    end

    context 'with an array and "attributes"' do
      it 'raises an error' do
        expect do
          described_class.new(type: :array, description: 'The bats', attributes: :bat_entity)
        end.to raise_error "Don't use 'attributes' on non-objects"
      end
    end
  end
end
