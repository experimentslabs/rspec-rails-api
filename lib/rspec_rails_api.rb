# frozen_string_literal: true

require 'rspec/rails/api/dsl/example_group'
require 'rspec/rails/api/dsl/example'
require 'rspec/rails/api/entity_config'
require 'rspec/rails/api/field_config'
require 'rspec/rails/api/matchers'
require 'rspec/rails/api/metadata'
require 'rspec/rails/api/open_api_renderer'
require 'rspec/rails/api/version'

module RSpec
  module Rails
    module Api
      class Error < StandardError
      end

      ##
      # OpenAPI types, format and Ruby class correspondence
      PARAM_TYPES = {
        int32:    { type: 'integer', format: 'int32', class: Integer },
        int64:    { type: 'integer', format: 'int64', class: Integer },
        float:    { type: 'number', format: 'float', class: Float },
        double:   { type: 'number', format: 'double', class: Float },
        byte:     { type: 'string', format: 'byte', class: String },
        binary:   { type: 'string', format: 'binary', class: String },
        date:     { type: 'string', format: 'date', class: String },
        datetime: { type: 'string', format: 'date-time', class: String },
        password: { type: 'string', format: 'password', class: String },
        boolean:  { type: 'boolean', format: nil },
        string:   { type: 'string', format: nil, class: String },
        integer:  { type: 'integer', format: nil, class: Integer },
        number:   { type: 'number', format: nil, class: Numeric },
        array:    { type: 'array', format: nil, class: Array },
        object:   { type: 'object', format: nil, class: Hash },
        file:     { type: 'string', format: 'binary' },
        # Not a real type, but we want this
        any:      {},
      }.freeze

      PRIMITIVES = PARAM_TYPES.keys
    end
  end
end
