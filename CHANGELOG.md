# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Quick remainder of the possible sections:
-----------------------------------------
Added, for new features.
Changed, for changes in existing functionality.
Deprecated, for soon-to-be removed features.
Removed, for now removed features.
Fixed, for any bug fixes.
Security, in case of vulnerabilities.
Maintenance, in case of rework, dependencies change
-->

## [Unreleased]

## [0.9.0] - 2024-12-26

### Added

- Support for entries of any type: use `:any` as type

## [0.8.3] - 2024-11-22

### Fixed

- Fixed documentation generation of the description on objects and arrays

## [0.8.2] - 2024-11-22

### Fixed

- Fixed documentation generation of object's attributes when they are not passed as reference.

## [0.8.1] - 2024-10-16

### Fixed

- Fixed expansion of nested object attributes defined with a symbol

## [0.8.0] - 2024-09-28

### Added

- Support for custom expected content types (as long as it's JSON): use `RSpec::Rails::Api::Metadata.default_expected_content_type=`
  in the spec helper, or on specific tests: `for_code 200, expect_one: :string, with_content_type: 'application/json+custom; charset=utf-8' do |url|`

### Changed

- [BREAKING] Target ruby 3.1.2 (debian 12 default)

## [0.7.0] - 2024-01-24

### Fixed

- [BREAKING] Don't add `name` property in security schemes that should not have one. 
  Only `type: :apiKey` should have one, so the `name` argument of `add_security_scheme` has been removed. Specify it
  when it is required/accepted only.
  ```rb
  # Change:
  #                            ref       name                      schema 
  renderer.add_security_scheme :basic,   "Basic authentication",   { type: "http", scheme: "basic" }
  renderer.add_security_scheme :api_key, "API key authentication", { type: "apiKey", in: "header" }
  # To:
  #                            ref       schema
  renderer.add_security_scheme :basic,   type: "http", scheme: "basic"
  renderer.add_security_scheme :api_key, name: "API key authentication", type: "apiKey", in: "header"
  ```

## [0.6.3] - 2024-01-24

### Fixed

- `required` fields/parameters now can be false
- Querystring parameters are better handled and documented: no more need to specify the whole querystring in URL

## [0.6.2] - 2024-01-17

### Fixed

- Renderer: handle arrays of object with inline attributes

## [0.6.1] - 2024-01-17

### Fixed

- Renderer: handle arrays of object with inline attributes (not a reference)
- Renderer: Remove the `name` attribute from `basic` authentication declaration
- Renderer: Add missing `items` key on arrays of any type 

## [0.6.0] - 2023-07-17

### Added

- Declaring the same resource multiple times is now possible
- Add a simple support for security schemes
- Add support for global entities declarations. Keep things DRYer :)
- `test_response_of`: add `ignore_content_type` flag to ignore response's content type
- `test_response_of`: add `ignore_response` flag to ignore tests on response
- Renderer: Add simple support for redactable content. It will replace content in responses by something else. For now, 
  sub-entities are not redacted.
- Support for `file` type. When declaring a field with the `file` type, it will change the request content-type to
  `multipart/form-data` automatically.

### Changed

- Generated JSON files will always be prettified to ease changes reviews when they are versioned
- When an unexpected 422 error happens, also display its content in error message
- [BREAKING] It is now impossible to declare the same entity twice. To remediate, rename your declared entities and/or
  use global declarations. Check [README](./README.md) for example.
- Renderer: Use response data as-is when it's not valid JSON

### Fixed

- Strip generated descriptions and summaries
- Primitives are no more referenced in schemas for "expect_one" types
- Compare type `:float` against `Numeric` class
- Entities: Raise error when using `:array` type with `attributes` property and `:object` type with `of` property
- Rendering of sub-references now use the correct reference
- Metadata: Add missing type on array parameters
- Don't transform parameters into JSON when making `get` requests
- Operation IDs don't use summaries for uniqueness
- Downcase response content type before comparison
- Type format is now added on request parameters if applicable

## [0.5.0] - 2023-01-02

### Changed

- Improved error messages
- Improved usage of primitive types: use `:string` instead of `:type_string`, and  more generally, remove the `type_`
  prefix

### Fixed

- Fixed an error when an object is defined with an attribute named `type`.

## [0.4.0] - 2021-12-19

### Added

- Check for arrays of primitives is now a thing:
  ```rb
  # In entities, when attribute is an array of primitives
  entity :user,
         aliases: { type: :array, description: 'Pseudonyms', of: :type_string }

  # In examples, when response is an array of primitives
  for_code 200, expect_many: :type_string do |url|
    #...
  end
  ```
- OpenApi:
  - Responses now includes the schema
  - `on_xxx` methods second parameter is now used as summary instead of description.
    Description can be defined on the third parameter.

### Changed

- All parameters attributes are considered required unless specified
- RSpec metadata is now stored in `rra` instead of `rrad` (the gem's first name
  was RSpec Rails API Doc at the time). Update RSpec configuration accordingly.
- DSL changes:
  - `visit` is renamed to `test_response_of`
  - Support for `doc_only` is removed
  - Response expectations _should_ now be declared with `for_code`, and should be
    removed from example bodies:
    ```rb
    # Before:
    for_code 200 do |url|
      visit url
      expect(response).to have_many defined: :post
    end
    
    # Now:
    for_code 200, expect_many: :post do |url|
      test_response_of url
    end
    ```

### Fixed

- Fix object `attributes` key in spec and documentation.
  When defining object attributes, documentation and tests used `properties` key
  while the code was waiting for an `attributes` key. The later makes more sense
  so the spec and documentation were fixed.

## [0.3.4] - 2021-10-20

### Added

- Add the "required" attribute in parameters

## [0.3.3] - 2021-06-02

### Fixed

- Fix correct types on request parameters

## [0.3.2] - 2021-03-09

### Changed

- Render examples results as YAML/JSON objects instead of text blocks.

### Fixed

- Fix YAML rendering (ruby objects were sometimes rendered in documentation)

## [0.3.1] - 2020-04-09

### Added

- Add support for "test only" examples, allowing to write examples without documentation.

## [0.3.0] - 2019-12-26

### Changed

- Rails 6 support, deprecated methods from Rails 5 are not supported. Use version `0.2.3` 
  of this gem if your application is still on 5.

## 0.2.3 - 2019-12-04

### Added

- Generated Swagger file now use the payloads of POST/PATCH/PUT requests.

### Changed

- Minimum Ruby version is now specified: Ruby 2.3.3.

## [0.2.2] - 2019-11-03

### Changed

- `for_code` method now have its `description` optional. If none is provided,
  the description will be set from the status code.

## [0.2.1] - 2019-11-03 [YANKED]

_Version 0.2.1 was released and yanked by mistake. Version 0.2.2 is the exact
same one, with a version bump_

## [0.2.0] - 2019-11-02

### Added

- `parameters` method to define path or requests params for the whole file.
- A simple Rails application now acts as example and is used to generate some
of the fixtures.

### Changed

- method `path_params` can now use a reference to a previously defined parameters
  set. The method has a new signature: `path_params(fields: nil, defined: nil)`. Update
  the existing calls accordingly. To use params defined with `parameters`, use the
  `defined` option: `path_params defined: :common_path_params`
- method `request_params` can now use a reference to a previously defined parameters
  set. The method has a new signature: `request_params(attributes: nil, defined: nil)`. Update
  the existing calls accordingly. To use params defined with `parameters`, use the
  `defined` option: `request_params defined: :common_form_params`

## [0.1.5] - 2019-10-31

### Fixed

- Fixed issue with POST/PUT/DELETE requests with no `request_params`
- Improved documentation (integration with Devise, typos and mistakes)

## [0.1.4] - 2019-10-24

### Added

- Added support for arrays of objects in request parameters

## [0.1.3] - 2019-10-23

### Added

- Added ability to document API descriptions, servers, etc... from the RSpec helper files

## [0.1.2] - 2019-10-22

### Added

- Added `item` property for arrays descriptions

## [0.1.1] - 2019-10-22

- Added support for custom headers in request examples (useful for `visit` method)

## [0.1.0] - 2019-10-21

Initial release
