# frozen_string_literal: true

module RSpec
  module Rails
    module Api
      module DSL
        # These methods will be available in examples (i.e.: 'for_code')
        module Example
          ##
          # Visits the current example and tests the response
          #
          # @param example             [Hash] Current example
          # @param path_params         [Hash] Path parameters definition
          # @param payload             [Hash] Request body
          # @param headers             [Hash] Custom headers
          # @param ignore_content_type [Boolean] Whether to ignore the response's content-type for this response only
          #
          # @return [void]
          def test_response_of(example, path_params: {}, payload: {}, headers: {}, ignore_content_type: false, ignore_response: false) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength, Metrics/ParameterLists, Layout/LineLength
            raise 'Missing context. Call "test_response_of" within a "for_code" block.' unless example

            status_code = prepare_status_code example.class.description

            request_params = prepare_request_params example.class.module_parent.description,
                                                    path_params, payload, headers

            send(request_params[:action],
                 request_params[:url],
                 params:  request_params[:params],
                 headers: request_params[:headers])

            check_response(response, status_code, ignore_content_type: ignore_content_type) unless ignore_response

            return if example.class.description.match?(/-> test (\d+)(.*)/)

            set_request_example example.class.metadata[:rra], request_params, status_code, response.body
          end

          private

          ##
          # Searches for a defined entity in example metadata or global entities
          #
          # If an entity needs expansion (e.g.: with an attribute like "type: :array, of: :something"), it will use the
          # scope where the entity was found: global entities or example metadata.
          #
          # @param entity [Symbol] Entity reference
          #
          # @return [RSpec::Rails::Api::EntityConfig, Hash] Defined entity
          def defined(entity)
            return { type: entity } if PRIMITIVES.include? entity

            current_resource = rra_metadata.current_resource
            raise '@current_resource is unset' unless current_resource

            definition = RSpec::Rails::Api::Metadata.entities[entity.to_sym]
            raise "Entity '#{entity}' was never defined (globally or in '#{current_resource}')" unless definition

            definition.expand_with(RSpec::Rails::Api::Metadata.entities)
          end

          ##
          # Performs various tests on the response
          #
          # @param response      [ActionDispatch::TestResponse] The response
          # @param expected_code [Number]                       Code to test for
          # @param ignore_content_type [Boolean]                Whether to ignore the response's content-type for
          #                                                     this response only
          def check_response(response, expected_code, ignore_content_type: false) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
            code_error_message = if response.status != expected_code && response.status == 422
                                   <<~TXT
                                     expected: #{expected_code}
                                          got: #{response.status}
                                     response: #{response.body}
                                   TXT
                                 end

            expect(response.status).to eq(expected_code), code_error_message
            if expected_code != 204 && !ignore_content_type
              content_type = rra_current_example[:expectations][:content_type] || RSpec::Rails::Api::Metadata.default_expected_content_type # rubocop:disable Layout/LineLength
              expect(response.headers['Content-Type'].downcase).to eq content_type
            end
            expectations = rra_current_example[:expectations]
            expect(response).to have_many defined(expectations[:many]) if expectations[:many]
            expect(response).to have_one defined(expectations[:one]) if expectations[:one]
            expect(response.body).to eq '' if expectations[:none]
          end

          ##
          # Adds a request example information to metadata
          #
          # @param rra_metadata  [RSpec::Rails::Api::Metadata]
          # @param request_params [Hash]
          # @param status_code    [Integer, nil]
          # @param response       [String, nil]
          #
          # @return [void]
          def set_request_example(rra_metadata, request_params, status_code = nil, response = nil)
            rra_metadata.add_request_example(url:         request_params[:example_url],
                                             action:      request_params[:action],
                                             status_code: status_code,
                                             response:    response,
                                             path_params: request_params[:path_params],
                                             params:      request_params[:params])
          end

          ##
          # Prepares the options for a request
          #
          # @param description     [String] RSpec example description
          # @param request_params  [Hash]   Request parameters
          # @param payload         [Hash]   Request body
          # @param request_headers [Hash]   Custom headers
          #
          # @return [Hash] Options for the request
          def prepare_request_params(description, request_params = {}, payload = {}, request_headers = {})
            example_params = description.split
            verb = example_params[0].downcase

            payload = payload.to_json if verb != 'get'

            {
              action:      verb,
              url:         prepare_request_url(example_params[1], request_params),
              example_url: example_params[1],
              params:      payload,
              headers:     prepare_request_headers(request_headers),
            }
          end

          ##
          # Replace path params by values and completes querystring
          #
          # @param url            [String] Url definition
          # @param request_params [Hash]   Request parameters
          #
          # @return [String] Actual path to visit
          def prepare_request_url(url, request_params) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
            used_path_params = []

            # Replace any ":xyz" in url
            url = url.gsub(/(?::(\w*))/) do |e|
              symbol = e.sub(':', '').to_sym
              used_path_params << symbol

              if request_params.key?(symbol)
                request_params[symbol]
              elsif respond_to?(symbol)
                send(symbol)
              else
                puts "! Define #{symbol} (let(:#{symbol}){ value }) or pass it to 'visit'"
              end
            end

            # Use everything else in query string
            uri = URI.parse(url)
            query_params = request_params.select { |k| used_path_params.exclude? k }
            uri.query = URI.encode_www_form(query_params) if query_params.present?

            uri.to_s
          end

          ##
          # Prepares request headers
          #
          # @param headers [Hash] Custom headers
          #
          # @return [Hash] Headers to use in request
          def prepare_request_headers(headers = {})
            {
              'Accept'       => 'application/json',
              'Content-Type' => 'application/json',
            }.merge headers
          end

          ##
          # Extracts status code from RSpec example description
          #
          # @param description [String] RSpec example description
          #
          # @return [Integer] Status code
          def prepare_status_code(description)
            code_match = /->(?: test)? (\d+) - .*/.match description

            raise 'Please provide a numerical code for the "for_code" block' unless code_match

            code_match[1].to_i
          end

          ##
          # Returns the current example configuration from metadata
          #
          # @return [Hash] Current example configuration
          def rra_current_example
            self.class.metadata[:rra_current_example]
          end

          ##
          # Returns the whole Rspec-rails-API metadata object
          #
          # @return [RSpec::Rails::Api::Metadata] Rspec-rails-API metadata object
          def rra_metadata
            self.class.metadata[:rra]
          end
        end
      end
    end
  end
end
