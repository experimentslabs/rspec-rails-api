# frozen_string_literal: true

module RSpec
  module Rails
    module Api
      VERSION = '0.9.0'
    end
  end
end
