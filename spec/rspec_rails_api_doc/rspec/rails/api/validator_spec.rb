# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rails/api/validator'

RSpec.describe RSpec::Rails::Api::Validator do
  let(:hash) { { a: 1, b: 2, c: { ca: 1, 'cb' => 2 } } }

  let(:structure) do
    {
      id:      { type: :integer, required: true, description: 'Identifier' },
      type:    { type: :string, required: true, description: 'The type' },
      content: { type: :string, required: false, description: 'The content' },
    }
  end

  describe '.check_type' do
    context 'when type is a boolean' do
      let(:type) { :boolean }

      context 'when value is valid' do
        it 'returns nil' do
          expect(described_class.send(:validate_type, true, type)).to be_nil
        end
      end

      context 'when value is invalid' do
        it 'returns the error message' do
          expect(described_class.send(:validate_type, 'something', type)).to eq 'is not a "boolean"'
        end
      end
    end

    context 'when type is "any"' do
      let(:type) { :any }

      it 'does not allow null values' do
        expect(described_class.send(:validate_type, nil, type)).to eq 'is null'
      end

      context 'when value is valid' do
        it 'returns nil' do
          expect(described_class.send(:validate_type, true, type)).to be_nil
        end
      end
    end

    context 'when type is supported' do
      let(:type) { :string }

      context 'when value is valid' do
        it 'returns nil' do
          expect(described_class.send(:validate_type, 'something', type)).to be_nil
        end
      end

      context 'when value is invalid' do
        it 'returns the error message' do
          expect(described_class.send(:validate_type, [], type)).to eq 'is not a "string"'
        end
      end
    end

    context 'when type is unsupported' do
      it 'raises an error' do
        expect do
          described_class.send :validate_type, 'something', :unsupported
        end.to raise_error('Unknown type unsupported')
      end
    end
  end

  describe '.validate_object' do
    # Use string keys, as in hashes from JSON responses
    let(:valid_entity) { { 'id' => 10 } }

    let(:entity_description) do
      RSpec::Rails::Api::EntityConfig.new(
        id:       { type: :integer, description: 'Identifier' },
        option:   { type: :object, required: false, description: 'Optional option' },
        posts:    { type: :array, required: false, description: 'List of posts', of: {
          id:    { type: :integer, description: 'Identifier' },
          title: { type: :string, description: 'Post title' },
        } },
        comments: { type: :array, required: false, description: 'List of comments', of: :string },
        author:   { type: :object, required: false, description: 'User object', attributes: {
          id:   { type: :integer, description: 'Identifier' },
          name: { type: :string, description: 'User name' },
        } }
      ).expand_with({})
    end

    it 'matches valid entity' do
      result = described_class.validate_object({ 'id' => 1, 'option' => {} }, entity_description)
      expect(result).to be_nil
    end

    it 'matches hashes without attributes defined' do
      result = described_class.validate_object valid_entity, entity_description
      expect(result).to be_nil
    end

    it 'matches valid entity with arrays of simple values' do
      valid_entity[:comments] = %w[super comments]
      result = described_class.validate_object valid_entity, entity_description

      expect(result).to be_nil
    end

    it 'matches valid entity with arrays of objects' do
      valid_entity[:posts] = [{ id: 1, title: 'The title' }]

      result = described_class.validate_object valid_entity, entity_description
      expect(result).to be_nil
    end

    it 'rejects incorrect values' do
      expected_error = { id: 'is not a "integer"' }
      entity = { 'id' => '10' }

      result = described_class.validate_object(entity, entity_description)
      expect(result).to eq expected_error
    end

    it 'rejects extra keys' do
      expected_error = { 'user' => 'is not defined' }
      entity = { 'id' => 10, 'user' => 'Bob' }

      result = described_class.validate_object(entity, entity_description)
      expect(result).to eq expected_error
    end

    it 'rejects missing required keys' do
      expected_error = { id: 'is missing' }
      entity = {}

      result = described_class.validate_object(entity, entity_description)
      expect(result).to eq expected_error
    end

    it 'handles errors in arrays of simple values' do
      expected_error = { comments: { '#1' => 'is not a "string"' } }
      entity = { 'id' => 10, 'comments' => ['valid', 10] }

      result = described_class.validate_object(entity, entity_description)
      expect(result).to eq expected_error
    end

    it 'handles errors in arrays of object values' do
      expected_error = { posts: { '#1' => { id: 'is not a "integer"', title: 'is missing' } } }
      entity = { 'id' => 10, 'posts' => [{ 'id' => 1, 'title' => 'The title' }, { 'id' => '2' }] }

      result = described_class.validate_object(entity, entity_description)
      expect(result).to eq expected_error
    end

    it 'handles errors in object values' do
      expected_error = { author: { id: 'is not a "integer"', name: 'is missing' } }
      entity = { 'id' => 10, 'author' => { 'id' => '1' } }

      result = described_class.validate_object(entity, entity_description)
      expect(result).to eq expected_error
    end
  end

  describe '.validate_array' do
    describe 'with simple values' do
      let(:entity_description) { { type: :string } }

      it 'validates every entry' do
        values = %w[hello world]
        expect(described_class.validate_array(values, entity_description)).to be_nil
      end

      it 'validates array without type for entries' do
        values = ['hello', 10]
        expect(described_class.validate_array(values, nil)).to be_nil
      end

      it 'returns an indexed error list' do
        values = ['hello', 10]
        expected = { '#1' => 'is not a "string"' }

        expect(described_class.validate_array(values, entity_description)).to eq expected
      end
    end

    describe 'with object values' do
      let(:entity_description) { { id: { type: :integer, required: true } } }

      it 'validates every entry' do
        values = [{ 'id' => 10 }, { 'id' => 11 }]
        expect(described_class.validate_array(values, entity_description)).to be_nil
      end

      it 'returns an indexed error list' do
        values = [{ 'id' => 10 }, 'hello', {}]
        expected = { '#1' => 'is not a hash', '#2' => { id: 'is missing' } }

        expect(described_class.validate_array(values, entity_description)).to eq expected
      end
    end
  end

  describe '.valid_type?' do
    it 'validates supported attributes' do
      expect(described_class.valid_type?(:integer)).to be true
    end

    it 'does not validate unsupported attributes' do
      expect(described_class.valid_type?(:type)).to be false
    end

    it 'handles a list of exception' do
      expect(described_class.valid_type?(:integer, except: [:integer])).to be false
    end
  end
end
