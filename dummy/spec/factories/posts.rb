# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    title { Faker::Books::Lovecraft.tome }
    content { Faker::Books::Lovecraft.paragraph }
  end
end
