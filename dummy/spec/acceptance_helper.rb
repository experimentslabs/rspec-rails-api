# frozen_string_literal: true

require "rspec_rails_api"
require "support/acceptance_entities"

RSpec.configure do |config|
  config.include RSpec::Rails::Api::DSL::Example
end

renderer = RSpec::Rails::Api::OpenApiRenderer.new
# renderer.api_servers = [{ url: 'https://example.com' }]
# renderer.api_title = 'A nice API for a nice application'
# renderer.api_version = '1'
# renderer.api_description = 'Access update data in this project'
# renderer.api_tos = 'http://example.com/tos.html'
# renderer.api_contact = { name: 'Admin', email: 'admin@example.com', 'http://example.com/contact' }
# renderer.api_license = { name: 'Apache', url: 'https://opensource.org/licenses/Apache-2.0' }
renderer.add_security_scheme :basic, { type: "http", scheme: "basic" }
renderer.add_security_scheme :api_key, name: "API key authentication", type: "apiKey", in: "header"
renderer.redact_responses custom_error: { context: "FILTERED" }

# RSpec::Rails::Api::Metadata.default_expected_content_type = 'application/json'

RSpec.configuration.after(:context, type: :acceptance) do |context|
  renderer.merge_context context.class.metadata[:rra].to_h, dump_metadata: true
end

RSpec.configuration.after(:suite) do
  # Default path is 'tmp/rspec_rails_api_output.json/yaml'
  renderer.write_files Rails.root.join("public", "swagger_doc"), only: [:yaml]
end

# Load all base entities
ACCEPTANCE_ENTITIES.each do |name, attributes|
  RSpec::Rails::Api::Metadata.add_entity name, attributes
end
